package truman.workshop.barcodescannerapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLHelper extends SQLiteOpenHelper {

	public SQLHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);		
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String sql ="create table if not exists goods(barcode varchar, name varchar, amtpuint integer,unitppackage integer,price integer)";		
		db.execSQL(sql);
		sql ="create table if not exists records(barcode varchar,saler varchar, price integer, idate varchar, comment varchar,timestamp long)";
		db.execSQL(sql);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldv, int newv) {
			db.execSQL("drop table records");
			db.execSQL("create table if not exists records(barcode varchar,saler varchar, price integer, idate varchar, comment varchar,timestamp long)");
	}

}
