package truman.workshop.barcodescannerapp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

public class IconTextListAdapter extends SimpleAdapter {
	private Context context;
	private String[] from;
	private int[] to;
	private int resource;
	private int imgndx=3;
	private List<? extends Map<String, ?>> data;
	public IconTextListAdapter(Context context,
			List<? extends Map<String, ?>> data, int resource, String[] from,
			int[] to) {
		super(context, data, resource, from, to);
		this.context = context;
		this.resource=resource;
		this.data = data;
		this.from=from;
		this.to=to;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
			//View rowView = inflater.inflate(R.layout.context_ic, parent, false);
		TextView[] texts = {new TextView(context),new TextView(context),new TextView(context),new TextView(context),new TextView(context),new TextView(context)}; 
		View rowView = inflater.inflate(resource, parent, false);
		@SuppressWarnings("unchecked")
		HashMap<String,String> map = (HashMap<String, String>)data.get(position);
		for(int i=0;i<imgndx;i++){
			 texts[i] = (TextView) rowView.findViewById(to[i]);			 
			 texts[i].setText(map.get(from[i]));
		}
		ImageView imageView = (ImageView) rowView.findViewById(to[imgndx]);
		if(imgndx<from.length){
		for(int i=imgndx+1;i<from.length;i++){
			 texts[i] = (TextView) rowView.findViewById(to[i]);
			 texts[i].setText(map.get(from[i]));
		}
		}
		if(map.get(from[imgndx]).matches("smile")){
			imageView.setImageResource(R.drawable.ic_smile);
		}else if(map.get(from[imgndx]).matches("what")){
			imageView.setImageResource(R.drawable.ic_what);
		} /*else {
			imageView.setImageResource(R.drawable.ic_blank);
		}*/
		return rowView;
	}
	public void setImgndx(int imgndx) {
		this.imgndx = imgndx;
	}
}
