package truman.workshop.barcodescannerapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class NewGood extends Activity implements OnClickListener {
	// private Button btnSave;
	private String barcode;
	// private EditText barCode;
	private AutoCompleteTextView itemName;
	private EditText price;
	private EditText specAmt;
	private EditText specUnits;
	private SQLHelper sqlite = new SQLHelper(this, "prices.db", null,
			MainActivity.DB_VER);
	private boolean newgood;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.newgood);
		EditText barCode = (EditText) findViewById(R.id.barcode);
		itemName = (AutoCompleteTextView) findViewById(R.id.itemNameNew);
		price = (EditText) findViewById(R.id.price);
		specAmt = (EditText) findViewById(R.id.specAmt);
		specUnits = (EditText) findViewById(R.id.specUnits);

		Button btnSave = (Button) findViewById(R.id.btnSave);
		btnSave.setOnClickListener(this);
		Bundle bundle = this.getIntent().getExtras();
		String tempName=bundle.getString("name");
		barcode = bundle.getString("barcode");
		barCode.setText(barcode);
		itemName.setText(tempName);
		newgood = bundle.getBoolean("newgood");
		initSuggestions();
		if (!newgood) {
			Button btnDel = (Button) findViewById(R.id.btnDel);
			btnDel.setOnClickListener(this);
			Button btnBack = (Button) findViewById(R.id.btnBack);
			btnBack.setOnClickListener(this);
			Button btnForward = (Button) findViewById(R.id.btnForward);
			btnForward.setOnClickListener(this);
			btnDel.setVisibility(View.VISIBLE);
			btnBack.setVisibility(View.VISIBLE);
			btnForward.setVisibility(View.VISIBLE);
			btnSave.setText("更新");
			bringData();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private void saveNewGood() {
		Long result = -1L;

		ContentValues values = new ContentValues();
		values.put("barcode", barcode.trim());
		values.put("name", itemName.getText().toString().trim());
		values.put("amtpuint",
				Integer.parseInt(specAmt.getText().toString().trim()));
		values.put("unitppackage",
				Integer.parseInt(specUnits.getText().toString().trim()));
		values.put("price", Integer.parseInt(price.getText().toString().trim()));

		SQLiteDatabase db = sqlite.getWritableDatabase();

		try {
			if (newgood) {
				result = db.insert("goods", null, values);
			} else {
				result = (long) db.update("goods", values,
						"barcode=" + barcode, null);
			}
			if (result > 0) {
				// Intent newIntent = new Intent();
				redirect(!newgood);
				/*
				 * if (newgood) { newIntent.setClass(NewGood.this,
				 * Record.class); newIntent.putExtra("barcode", barcode);
				 * newIntent.putExtra("name", itemName.getText());
				 * newIntent.putExtra("new", true); } else {
				 * newIntent.setClass(NewGood.this, MainActivity.class);
				 * newIntent.putExtra("rescan", false); }
				 * startActivity(newIntent);
				 */
			}
		} catch (Exception e) {
			Toast toast = Toast.makeText(getApplicationContext(), "Exception!"
					+ e.getMessage(), Toast.LENGTH_SHORT);
			toast.show();
		} finally {
			db.close();
			NewGood.this.finish();
		}
	}

	private void delGood(int option) {
		SQLiteDatabase db = sqlite.getWritableDatabase();
		String sql = "delete from ";
		Cursor result = null;
		switch (option) {
		case 1:
			sql = "delete from records where barcode='" + barcode + "'";
			result = db.rawQuery(sql, null);
			break;
		case 2:
			sql = "delete from goods where barcode='" + barcode + "'";
			result = db.rawQuery(sql, null);
			break;
		}
		if (result.getCount() > 0) {
			Toast.makeText(NewGood.this, "==" + result.getCount() + "筆資料已刪除==",
					Toast.LENGTH_SHORT).show();
		}

		if (option == 2) {
			db.close();
			redirect(true);
		}
	}

	private void initSuggestions() {
		String sql = "select distinct name from goods";
		SQLiteDatabase db = sqlite.getReadableDatabase();
		Cursor result = db.rawQuery(sql, null);

		String strList = "";
		for (result.moveToFirst(); !result.isAfterLast(); result.moveToNext()) {
			strList = strList.concat(result.getString(0)).concat(",");
		}
		if (strList.length() > 1) {
			strList = strList.substring(0, strList.length() - 1);
		}
		String[] suggestions = strList.split(",");
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_expandable_list_item_1, suggestions);
		itemName.setAdapter(adapter);
		itemName.setTextColor(Color.BLACK);
		db.close();
	}

	private void bringData() {
		String sql = "select * from goods where barcode='" + barcode + "'";
		SQLiteDatabase db = sqlite.getReadableDatabase();
		Cursor result = db.rawQuery(sql, null);
		result.moveToNext();
		int colndx = -1;
		colndx = result.getColumnIndex("name");
		itemName.setText(colndx >= 0 ? result.getString(colndx) : "");
		colndx = result.getColumnIndex("price");
		price.setText(colndx >= 0 ? result.getString(colndx) : "");
		colndx = result.getColumnIndex("amtpuint");
		specAmt.setText(colndx >= 0 ? result.getString(colndx) : "");
		colndx = result.getColumnIndex("unitppackage");
		specUnits.setText(colndx >= 0 ? result.getString(colndx) : "");
		result.close();
		db.close();
	}

	private void redirect(boolean back) {
		Intent newIntent = new Intent();
		if (back) {
			newIntent.setClass(NewGood.this, MainActivity.class);
			newIntent.putExtra("rescan", false);
		} else {
			newIntent.setClass(NewGood.this, Record.class);
			newIntent.putExtra("barcode", barcode);
			newIntent.putExtra("name", itemName.getText());
			//newIntent.putExtra("new", true);
		}
		startActivity(newIntent);
		NewGood.this.finish();
	}

	private AlertDialog getAlertDialog(String title, String message) {
		// 產生一個Builder物件
		Builder builder = new AlertDialog.Builder(NewGood.this);
		// 設定Dialog的標題
		builder.setTitle(title);
		// 設定Dialog的內容
		builder.setMessage(message);
		// 設定Positive按鈕資料
		builder.setPositiveButton(R.string.delGoodOnly,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						delGood(2);
					}
				});
		// 設定Negative按鈕資料
		builder.setNeutralButton(R.string.cancel,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// 按下按鈕時顯示快顯
						Toast.makeText(NewGood.this, "==取消==",
								Toast.LENGTH_SHORT).show();
					}
				});
		builder.setNegativeButton(R.string.delAll,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						delGood(1);
						delGood(2);
					}
				});
		// 利用Builder物件建立AlertDialog
		return builder.create();
	}

	@Override
	public void onClick(View button) {
		switch (button.getId()) {
		case R.id.btnSave:
			saveNewGood();
			break;
		case R.id.btnDel:
			AlertDialog alertDialog = getAlertDialog("請選擇",
					"只刪除物品內容或者相關記錄一併刪除？");
			alertDialog.show();
			break;
		case R.id.btnBack:
			redirect(true);
			break;
		case R.id.btnForward:
			redirect(false);
			break;

		}
	}
}
