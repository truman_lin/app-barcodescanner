package truman.workshop.barcodescannerapp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Record extends Activity implements OnClickListener,
		OnItemClickListener, OnDateSetListener , OnItemLongClickListener{
	private String barcode;

	private EditText edtSaler, idate, edtSprice;
	private AutoCompleteTextView edtComment;
	private Spinner cmbSalers;
	private ListView recordList;
	private Button btnRecord;
	private SQLHelper sqlite = new SQLHelper(this, "prices.db", null,
			MainActivity.DB_VER);
	private List<String> salers = new ArrayList<String>();
	private ArrayList<HashMap<String, String>> records = new ArrayList<HashMap<String, String>>();
	private HashMap<String, String> selectedItem;
	private SQLiteDatabase db;
	private boolean recorded = false;
	//private boolean newdata = true;

	/**
	 * on create
	 * 
	 * @param saveInstnaceState
	 *            barcode,name
	 * 
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.record);

		recorded = false;
		Bundle bundle = this.getIntent().getExtras();
		barcode = bundle.getString("barcode");
		//newdata = bundle.getBoolean("new");

		TextView itemName = (TextView) findViewById(R.id.txtItemName);
		itemName.setText(bundle.get("name").toString());
		cmbSalers = (Spinner) findViewById(R.id.cmbSalers);
		edtSaler = (EditText) findViewById(R.id.edtSaler);
		edtSprice = (EditText) findViewById(R.id.edtSPrice);
		idate = (EditText) findViewById(R.id.edtDate);
		edtComment = (AutoCompleteTextView) findViewById(R.id.edtComment);		
		btnRecord = (Button) findViewById(R.id.btnRecord);
		//if (newdata) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", Locale.US);
			idate.setText(sdf.format(new Date()));
		//} else {
		//	btnRecord.setText("更新");
		//}
		btnRecord.setOnClickListener(this);
		Button btnRescan = (Button) findViewById(R.id.btnReScan);
		btnRescan.setOnClickListener(this);
		Button btnEnd = (Button) findViewById(R.id.btnEnd);
		btnEnd.setOnClickListener(this);
		Button btnQuery = (Button) findViewById(R.id.btnQuery);
		btnQuery.setOnClickListener(this);
		ImageButton btnDate = (ImageButton) findViewById(R.id.btnDate);
		btnDate.setOnClickListener(this);
		edtSaler.setOnClickListener(this);
		initSpinner();
		recordList = (ListView) findViewById(R.id.lstRecords);
		recordList.setOnItemLongClickListener(this);
		recordList.setOnItemClickListener(this);
		initRecordList();
		initSuggestions();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnReScan:
			redirect(true);
			break;
		case R.id.btnEnd:
			this.finish();
			break;
		case R.id.btnRecord:
			if (!recorded) {
				recordit();
			}
			break;
		case R.id.btnQuery:
			redirect(false);
			break;
		case R.id.edtSaler:
			edtSaler.selectAll();
			break;
		case R.id.btnDate:
			int iyear,
			imonth,
			iday;
			String strDate = idate.getText().toString();
			if (strDate.trim() == "") {
				Calendar cal = Calendar.getInstance();
				iyear = cal.get(Calendar.YEAR);
				imonth = cal.get(Calendar.MONTH);
				iday = cal.get(Calendar.DAY_OF_MONTH);
			} else {
				iyear = Integer.parseInt(strDate.substring(0, 4));
				imonth = Integer.parseInt(strDate.substring(4, 6));
				iday = Integer.parseInt(strDate.substring(6));
			}
			DatePickerDialog dpd = new DatePickerDialog(this, this, iyear,
					imonth - 1, iday);
			dpd.show();
			break;
		}
	}

	private void initSpinner() {
		salers.add("大潤發");
		salers.add("家樂福");
		String sql = "select distinct saler from records where saler not in ('大潤發','家樂福')";
		db = sqlite.getReadableDatabase();
		Cursor result = db.rawQuery(sql, null);
		int index = result.getColumnIndex("saler");
		for (result.moveToFirst(); !result.isAfterLast(); result.moveToNext()) {
			salers.add(result.getString(index));
		}
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, salers);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		Spinner cmbSalers = (Spinner) findViewById(R.id.cmbSalers);
		cmbSalers.setAdapter(adapter);
		cmbSalers.setOnItemSelectedListener(spinnerListener);
		db.close();
	}

	private long fillRecords(String sql,String imgndx) {
		long retValue = 0;		
		Cursor result = db.rawQuery(sql, null);
		if (result.getCount() > 0) {
			result.moveToFirst();
			retValue = result.getLong(result.getColumnIndex("timestamp"));
			for (; !result.isAfterLast(); result.moveToNext()) {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("saler",
						result.getString(result.getColumnIndex("saler")));
				map.put("idate",
						result.getString(result.getColumnIndex("idate")));
				map.put("price",
						result.getString(result.getColumnIndex("price")));
				map.put("comment",
						result.getString(result.getColumnIndex("comment")));
				map.put("timestamp",
						result.getString(result.getColumnIndex("timestamp")));
				map.put("imgndx", imgndx);
				double amtpunit = result.getDouble(result
						.getColumnIndex("amtpuint"));
				double unitppackage = result.getDouble(result
						.getColumnIndex("unitppackage"));
				double price = result.getDouble(result.getColumnIndex("price"));
				String punit = result.getString(result.getColumnIndex("price"));
				punit = String.format(Locale.US, "%1$,.4f", price
						/ (amtpunit * unitppackage));
				map.put("punit", punit);
				records.add(map);
			}
		}
		result.close();
		return retValue;
	}

	private void initRecordList() {
		long lv1 = 0,lv2=0;
		/*
		 * Log.d("initRecordList",
		 * "************=== initializing record list ===************");
		 */
		records.clear();
		String sql = "select r.*,g.amtpuint,g.unitppackage from records r,goods g where r.barcode='"
				+ barcode + "' and r.barcode=g.barcode ";
		String sql1 = "from records where barcode='" + barcode + "'";
		db = sqlite.getReadableDatabase();
		lv1=fillRecords(sql + "and r.timestamp=(select timestamp " + sql1
				+ " and price=(select min(price) " + sql1 + ")) limit 1","smile");
		if(lv1>0){
		 lv2=fillRecords(sql + "and r.timestamp=(select timestamp " + sql1
				+ " and price=(select max(price) " + sql1 + " and timestamp<>"+lv1+")) limit 1","what");
		}
		if(lv1!=0&&lv2!=0){
			fillRecords(sql+"and r.timestamp not in("+lv1+","+lv2+") order by r.timestamp desc limit 3","ok");
		}
		
		ListAdapter lstAdapter = new IconTextListAdapter(this, records,
				R.layout.context_ic, new String[] { "saler", "price", "punit","imgndx",
						"comment", "idate" }, new int[] { R.id.cCol1,
						R.id.cCol2, R.id.cCol3, R.id.imgNdx,R.id.cCol4, R.id.cCol5 });
		((IconTextListAdapter) lstAdapter).setImgndx(3);
		/*
		 * new SimpleAdapter(this, records, R.layout.context, new String[] {
		 * "saler", "price","punit", "comment", "idate" }, new int[] {
		 * R.id.cCol1, R.id.cCol2, R.id.cCol3, R.id.cCol4,R.id.cCol5 });
		 */recordList.setAdapter(lstAdapter);
		 db.close();
		if (records.size() > 0){// & !newdata) {
			bringData(records.get(0));
			selectedItem = records.get(0);
		}
	}

	private void bringData(HashMap<String, String> map) {
		edtSaler.setText(map.get("saler"));
		edtSprice.setText(map.get("price"));
		idate.setText(map.get("idate"));
		edtComment.setText(map.get("comment"));
		String strSaler = map.get("saler");
		for (int i = 0; i < cmbSalers.getCount(); i++) {
			if (strSaler.equals(cmbSalers.getAdapter().getItem(i))) {
				cmbSalers.setSelection(i);
				break;
			}
		}
	}

	private void initSuggestions() {
		String sql = "select distinct comment from records";
		SQLiteDatabase db = sqlite.getReadableDatabase();
		Cursor result = db.rawQuery(sql, null);

		String strList = "";
		for (result.moveToFirst(); !result.isAfterLast(); result.moveToNext()) {
			strList = strList.concat(result.getString(0)).concat(",");
		}
		if (strList.length() > 1) {
			strList = strList.substring(0, strList.length() - 1);
		}
		String[] suggestions = strList.split(",");
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_expandable_list_item_1, suggestions);
		edtComment.setAdapter(adapter);
		edtComment.setTextColor(Color.BLACK);
		db.close();
	}

	private void recordit() {
		db = sqlite.getWritableDatabase();
		Long result = -1L;

		ContentValues values = new ContentValues();
		values.put("barcode", barcode);
		values.put("saler", edtSaler.getText().toString());
		values.put("price", Integer.parseInt(edtSprice.getText().toString()
				.trim().length() == 0 ? "0" : edtSprice.getText().toString()
				.trim()));
		values.put("idate", idate.getText().toString());
		values.put("comment", edtComment.getText().toString());
		values.put("timestamp", String.valueOf(new Date().getTime()));
		try {
			if ("儲存".equals(btnRecord.getText().toString())) {
				String sql="select * from records where barcode='"+barcode+"' and saler='"+values.getAsString("saler")+"' and price="+values.getAsString("price")+
						" and idate='"+values.getAsString("idate")+"' and comment='"+values.getAsString("comment")+"'";
				if(dataExisted(sql)){
					Toast toast = Toast.makeText(getApplicationContext(),
							"=資料已存在=", Toast.LENGTH_SHORT);
					toast.show();
				}else{
				result = db.insert("records", null, values);
				}
			} else {
				result = (long) db.update("records", values, "timestamp="
						+ selectedItem.get("timestamp"), null);
			}

			if (result > 0) {
				//newdata = false;
				Toast toast = Toast.makeText(getApplicationContext(),
						"=資料已儲存=", Toast.LENGTH_SHORT);
				toast.show();
				if (!salers.contains(edtSaler.getText().toString())) {
					Spinner cmbSalers = (Spinner) findViewById(R.id.cmbSalers);
					salers.add(edtSaler.getText().toString());
					cmbSalers.requestLayout();
				}
				// initRecordList();

				HashMap<String, String> map = new HashMap<String, String>();
				map.put("barcode", barcode);
				map.put("saler", values.getAsString("saler"));
				map.put("price", values.getAsString("price"));
				map.put("idate", values.getAsString("idate"));
				map.put("comment", values.getAsString("comment"));
				map.put("timestamp", values.getAsString("timestamp"));
				// if (newdata) {
				// records.add(map);
				// } else {
				// records.clear();
				initRecordList();
				// }
				recordList.requestLayout();
				recorded = true;
			}
		} catch (Exception e) {
			Toast toast = Toast.makeText(getApplicationContext(), "Exception!"
					+ e.getMessage(), Toast.LENGTH_SHORT);
			toast.show();
		} finally {
			db.close();
		}
	}

	private boolean dataExisted(String sql){
		int result=-1;
		
		db=sqlite.getReadableDatabase();
		Cursor resultSet = db.rawQuery(sql, null);
		result = resultSet.getCount();
		resultSet.close();
		db.close();
		return result>0;
	}
	private Spinner.OnItemSelectedListener spinnerListener = new Spinner.OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> adapterView, View arg1,
				int arg2, long arg3) {
			edtSaler.setText(adapterView.getSelectedItem().toString());
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// then do nothing.
		}

	};

	private void redirect(boolean rescan) {
		/*
		 * if (!recorded) { recordit(); }
		 */
		if(db.isOpen()){
			db.close();
		}
		Intent newIntent = new Intent();
		newIntent.setClass(Record.this, MainActivity.class);
		newIntent.putExtra("rescan", rescan);
		startActivity(newIntent);
		Record.this.finish();
	}

	@Override
	public void onItemClick(AdapterView<?> lstAdapter, View view, int position,
			long id) {
			@SuppressWarnings("unchecked")
			HashMap<String, String> map = (HashMap<String, String>) lstAdapter
					.getAdapter().getItem(position);

			bringData(map);
//			recorded = false;
			selectedItem = map;
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		idate.setText(String.valueOf(year)
				+ String.format("%02d", monthOfYear + 1)
				+ String.format("%02d", dayOfMonth));
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> lstAdapter, View view, int position,
			long id) {
		@SuppressWarnings("unchecked")
		HashMap<String, String> map = (HashMap<String, String>) lstAdapter
				.getAdapter().getItem(position);

		bringData(map);
		btnRecord.setText("更新");
		//newdata = false;
		recorded = false;
		selectedItem = map;

		return false;
	}
}
