package truman.workshop.barcodescannerapp;

import java.util.Calendar;

import android.app.DatePickerDialog.OnDateSetListener;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;


public class DatePickerFragment extends DialogFragment implements
		OnDateSetListener {
	private String strDate;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the current date as the default date in the picker
		Calendar cal = Calendar.getInstance();
		// Create a new instance of DatePickerDialog and return it
		return new DatePickerDialog(getActivity(), this,
				cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
				cal.get(Calendar.DAY_OF_MONTH));

	}

	public void onDateSet(DatePicker view, int year, int month, int day) {
		strDate = String.valueOf(year) + String.format("%02d", month)
				+ String.format("%02d", day);
	}

	public String getStrDate() {
		return strDate;
	}
}
