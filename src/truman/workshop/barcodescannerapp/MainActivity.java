package truman.workshop.barcodescannerapp;

import java.util.ArrayList;
import java.util.HashMap;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class MainActivity extends Activity implements OnClickListener,
		OnItemClickListener, OnItemLongClickListener {
	private ListView lstHeader, lstGoods;
	private SQLHelper sqlite = new SQLHelper(this, "prices.db", null, DB_VER);
	private ArrayList<HashMap<String, String>> records = new ArrayList<HashMap<String, String>>();
	private String barcode;
	public static int DB_VER = 6;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		Button scanBtn = (Button) findViewById(R.id.btnScan);
		Button btnEnd = (Button) findViewById(R.id.btnEnd);
		lstGoods = (ListView) findViewById(R.id.lstGoods);
		lstHeader = (ListView) findViewById(R.id.lstHeader);
		scanBtn.setOnClickListener(this);
		btnEnd.setOnClickListener(this);
		lstHeader.setOnItemLongClickListener(this);
		lstGoods.setOnItemClickListener(this);
		lstGoods.setOnItemLongClickListener(this);
		fillRecords(records, "all");
		initList(records, lstHeader, lstGoods);
		Bundle bundle = this.getIntent().getExtras();
		if (bundle != null) {
			if (bundle.getBoolean("rescan")) {
				doScan();
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnScan:
			doScan();
			break;
		case R.id.btnEnd:
			this.finish();
			break;
		}
	}

	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		// retrieve scan result
		IntentResult scanningResult = IntentIntegrator.parseActivityResult(
				requestCode, resultCode, intent);
		if (scanningResult != null) {
			// we have a result
			barcode = scanningResult.getContents();
			String tempName=scanningResult.getFormatName();
			byte[] temp = scanningResult.getRawBytes();
			if(temp!=null) tempName=temp.toString();
			
			String sql = "select name from goods where barcode='" + barcode
					+ "'";
			Intent newIntent = new Intent();
			SQLiteDatabase db = sqlite.getReadableDatabase();
			Cursor result = db.rawQuery(sql, null);

			if (result.getCount() == 0) {
				newIntent.setClass(MainActivity.this, NewGood.class);
				newIntent.putExtra("barcode", barcode);
				newIntent.putExtra("newgood", true);
				newIntent.putExtra("name", tempName);
			} else {
				result.moveToNext();
				int nameIndex = result.getColumnIndex("name");
				newIntent.setClass(MainActivity.this, Record.class);
				newIntent.putExtra("barcode", barcode);
				newIntent.putExtra("name", result.getString(nameIndex));
				//newIntent.putExtra("new", true);
			}
			startActivity(newIntent);
			MainActivity.this.finish();
			result.close();
			db.close();
		} else {
			Toast toast = Toast.makeText(getApplicationContext(),
					"No scan data received!", Toast.LENGTH_SHORT);
			toast.show();
		}
	}

	private void doScan() {
		IntentIntegrator scanIntegrator = new IntentIntegrator(this);
		scanIntegrator.initiateScan();
	}

	private void fillRecords(ArrayList<HashMap<String, String>> records,
			String range) {
		String sql = "select * from goods order by name";
		SQLiteDatabase db = sqlite.getReadableDatabase();
		if (!"all".matches(range)) {
			sql = "select * from goods where name like '%" + range
					+ "%' order by name";
		}
		Cursor result = db.rawQuery(sql, null);
		records.clear();
		for (result.moveToFirst(); !result.isAfterLast(); result.moveToNext()) {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("barcode",
					result.getString(result.getColumnIndex("barcode")));
			map.put("name", result.getString(result.getColumnIndex("name")));
			map.put("spec",
					result.getString(result.getColumnIndex("amtpuint"))
							.concat("X")
							.concat(result.getString(result
									.getColumnIndex("unitppackage"))));
			map.put("price", result.getString(result.getColumnIndex("price")));
			records.add(map);
		}
		result.close();
		db.close();
	}

	/**
	 * initialize the list get the goods, onSelected redirect to the recording
	 * page barcode,itemName, spec,price
	 */
	private void initList(ArrayList<HashMap<String, String>> records,
			ListView lstHeader, ListView lstGoods) {
		int[] intsCover = new int[] { R.id.fCol1, R.id.fCol2, R.id.fCol3,
				R.id.fCol4 };
		String[] strsCover = new String[] { "name", "barcode", "spec", "price" };
		ArrayList<HashMap<String, String>> headers = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> hmap = new HashMap<String, String>();
		hmap.put("barcode", "Barcode");
		hmap.put("name", "品名");
		hmap.put("spec", "規格");
		hmap.put("price", "價格");
		headers.add(hmap);
		ListAdapter lstAdapter = new SimpleAdapter(this, records,
				R.layout.cover, strsCover, intsCover);
		ListAdapter hAdapter = new SimpleAdapter(this, headers, R.layout.cover,
				strsCover, intsCover);
		lstHeader.setAdapter(hAdapter);
		lstGoods.setAdapter(lstAdapter);
	}

	@Override
	public void onItemClick(AdapterView<?> lstAdapter, View view, int position,
			long id) {
		@SuppressWarnings("unchecked")
		HashMap<String, String> map = (HashMap<String, String>) lstAdapter
				.getAdapter().getItem(position);
		Intent newIntent = new Intent();
		newIntent.setClass(MainActivity.this, Record.class);
		newIntent.putExtra("barcode", map.get("barcode"));
		newIntent.putExtra("name", map.get("name"));
		//newIntent.putExtra("new", true);
		startActivity(newIntent);
		MainActivity.this.finish();
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> lstAdapter, View view,
			int position, long id) {
		if (lstAdapter.getId() == R.id.lstHeader) {
			AlertDialog alertDialog = getAlertDialog("尋找...", "輸入商品名稱");
			alertDialog.show();
		} else {
			@SuppressWarnings("unchecked")
			HashMap<String, String> map = (HashMap<String, String>) lstAdapter
					.getAdapter().getItem(position);
			Intent newIntent = new Intent();
			newIntent.setClass(MainActivity.this, NewGood.class);
			newIntent.putExtra("barcode", map.get("barcode"));
			newIntent.putExtra("newgood", false);
			startActivity(newIntent);
			MainActivity.this.finish();
		}
		return false;
	}

	public TextWatcher getTextWatcher() {
		return new TextWatcher() {

			public void afterTextChanged(Editable s) {
				Log.d("afterTextChanged", "s=" + s.toString());
				fillRecords(records, s.toString());
				initList(records, lstHeader, lstGoods);
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}
		};

	}

	private AlertDialog getAlertDialog(String title, String message) {
		LayoutInflater inflater = this.getLayoutInflater();

		// 產生一個Builder物件
		Builder builder = new AlertDialog.Builder(MainActivity.this);
		View boxView = inflater.inflate(R.layout.searchbox, null);
		builder.setView(boxView);
		builder.setTitle(title);
		builder.setMessage(message);
		final EditText edtSearch = (EditText) boxView.findViewById(R.id.edtSearch);
		edtSearch.addTextChangedListener(new TextWatcher() {
	          public void afterTextChanged(Editable s) {
					fillRecords(records, edtSearch.getText().toString());
					initList(records, lstHeader, lstGoods);
	          }

	          public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

	          public void onTextChanged(CharSequence s, int start, int before, int count) {}
	       });
		builder.setPositiveButton(R.string.confirm,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
					}
				});
		builder.setNegativeButton(R.string.cancel,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						fillRecords(records, "all");
						initList(records, lstHeader, lstGoods);
					}
				});
		// 利用Builder物件建立AlertDialog
		return builder.create();
	}

}
